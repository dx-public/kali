#!/bin/bash

echo "Checking if Google Chrome is installed"
installed=`dpkg-query -l | grep 'google-chrome'`

if [ ! -z "$installed" ]; then
echo "Google Chrome is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        installed='';;
    u|U)
        echo "uninstalling"
        apt -y remove google-chrome*;;
    *)
        installed='n';;
esac

fi

if [ -z "$installed" ]; then

echo "Downloadng Chrome"
curl -s -L https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o /tmp/chrome.deb

echo "Installing Chrome"
sudo dpkg -i /tmp/chrome.deb

fi


echo "DONE going back to menu"
