#!/bin/env bash

#if unable to update#
#mv /etc/apt/apt.conf.d/20listchanges /etc/.
#apt --fix-broken install
#apt reinstall python3-minimal apt-listchanges
#mv /etc/20listchanges /etc/apt/apt.conf.d/20listchanges

whichalt(){
file=$1
found=0
if [ -z "$file" ]; then return 1; fi
pathdir=$(echo $PATH | awk '/[a-zA-Z0-9]/')
if [ -z "$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1"; fi
for p in $(echo $pathdir | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "${p}/${file}" ]; then found=1; echo "${p}/${file}"; return 0; fi
 done
if [ "$found"==0 ]; then echo "$file not found"; return 1; fi
}

echo "-----------------"
echo "Reclaiming free space"
echo "-----------------"

#Tweaks to free space
if [ "$(whichalt journalctl)" != 'journalctl not found' ]; then $(whichalt journalctl) --vacuum-size=500M; fi

echo "-----------------"
echo "Updating cron files"
echo "-----------------"

if [ ! -d /var/log/cron ]; then mkdir -p /var/log/cron; fi
if [ ! -f /etc/crontab ]; then echo "creating crontab file"; touch /etc/crontab; fi

echo "Changing Static Crontab Values"
minh=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
mind=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
minw=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
minm=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
hrsd=`awk -v min=0 -v max=5 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`

if [ -z "$(awk '/[a-zA-Z0-9]/' /etc/crontab)" ]; then
echo "crontab blank... recreating default crontab"
cat > /etc/crontab <<'EOF'
MAILTO=''
* * * * * root cd / && run-parts --report /etc/cron.minutes
17 * * * * root cd / && run-parts --report /etc/cron.hourly
25 6 * * * root cd / && run-parts --report /etc/cron.daily
47 6 * * 7 root cd / && run-parts --report /etc/cron.weekly
52 6 1 * * root cd / && run-parts --report /etc/cron.monthly
@reboot root cd / && run-parts --report /etc/cron.startup > /var/log/cron/startup.log
EOF
fi

if [ ! -d /etc/cron.startup ]; then mkdir -p /etc/cron.startup; fi
if [ -z "$(awk '/MAILTO/' /etc/crontab)" ]; then echo "--adding mailto value"; echo "$(awk 'BEGIN {print "MAILTO=\"\"" } {print}' /etc/crontab)" > /etc/crontab; fi

#create minutes
if [ ! -d /etc/cron.minutes ]; then mkdir /etc/cron.minutes; fi
if [ ! "$(awk '/cron.minutes/' /etc/crontab)" ]; then echo '--adding minutes value'; echo '* * * * * root cd / && run-parts --report /etc/cron.minutes' >> /etc/crontab; fi
#create hourly
if [ ! -d /etc/cron.hourly ]; then mkdir /etc/cron.hourly; fi
if [ ! "$(awk '/cron.hourly/' /etc/crontab)" ]; then echo '--adding hourly value'; echo '17 * * * * root cd / && run-parts --report /etc/cron.hourly' >> /etc/crontab; fi
#create daily
if [ ! -d /etc/cron.daily ]; then mkdir /etc/cron.daily; fi
if [ ! "$(awk '/cron.daily/' /etc/crontab)" ]; then echo '--adding daily value'; echo '25 6 * * * root cd / && run-parts --report /etc/cron.daily' >> /etc/crontab; fi
#create weekly
if [ ! -d /etc/cron.weekly ]; then mkdir /etc/cron.weekly; fi
if [ ! "$(awk '/cron.weekly/' /etc/crontab)" ]; then echo '--adding weekly value'; echo '47 6 * * 7 root cd / && run-parts --report /etc/cron.weekly' >> /etc/crontab; fi
#create monthly
if [ ! -d /etc/cron.monthly ]; then mkdir /etc/cron.monthly; fi
if [ ! "$(awk '/cron.monthly/' /etc/crontab)" ]; then echo '--adding monthly value'; echo '52 6 1 * * root cd / && run-parts --report /etc/cron.monthly' >> /etc/crontab; fi

if [ -z "$(awk '/@reboot/' /etc/crontab)" ]; then echo "--adding reboot value"; echo '@reboot root cd / && run-parts --report /etc/cron.startup > /var/log/cron/startup.log' >> /etc/crontab; fi

echo "randomizing default cron values"
echo "$(awk '{gsub(/17/,"'$minh'");gsub(/25/,"'$mind'");gsub(/47/,"'$minw'");gsub(/52/,"'$minm'");gsub(/6/,"'$hrsd'");print}' /etc/crontab)" > /etc/crontab

#change back to default permissions so crontab can run correctly
chmod 644 /etc/crontab

ONLINE="notonline"
echo "-----------------"
echo "Checking if online"
echo "-----------------"
curlbin=$(whichalt curl)
if ! whichalt curl >/dev/null; then echo "curl not installed. Installing..."; apt install -y curl; fi
onlineserver=$($curlbin -sI --connect-timeout 4.1 https://1.1.1.1)
if [ -z "$onlineserver" ]; then onlineserver=$($curlbin -sI --connect-timeout 4.1 https://9.9.9.9); fi
if [ -z "$onlineserver" ]; then onlineserver=$($curlbin -sI --connect-timeout 4.1 https://[2606:4700:4700::1111]); fi
if [ -z "$onlineserver" ]; then onlineserver=$($curlbin -sI --connect-timeout 4.1 https://[2620:fe::fe]); fi
if [ -z "$onlineserver" ]; then onlineserver=$($curlbin -sI --connect-timeout 4.1 https://nist.time.gov); fi

#if $(whichalt curl) -sI --fail https://1.1.1.1 -o /dev/null; then
#if curl -sI --fail https://nist.time.gov -o /dev/null; then
if [ ! -z "$onlineserver" ]; then
 echo "ONLINE... continuing..."
 ONLINE=""
else
 echo "OFFLINE... skipping..."
 ONLINE="notonline"
fi

if [ -z "$ONLINE" ]; then
echo "-----------------"
echo "Sync Time"
echo "-----------------"

if [ "$(whichalt timedatectl)" != 'timedatectl not found' ]; then $(whichalt timedatectl) set-ntp false; fi
if [ "$(whichalt date)" != 'date not found' ]; then $(whichalt date) -s "$(echo "$onlineserver" | $(whichalt awk) '/[dD]ate: / { $1=""; print }' | $(whichalt awk) '{gsub(/^ | $/,""); print}')"; fi
#date -s "`curl --connect-timeout 4.1 -sI 'https://time.cloudflare-dns.com/' 2>/dev/null | grep -i '^date:' | sed 's/^[Dd]ate: //g'`"
#date -s "`curl --connect-timeout 4.1 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print $2}'`"
#date -s "$(curl -sI --connect-timeout 4.1 https://1.1.1.1 | awk '/[dD]ate: / { $1=""; print }' | awk '{gsub(/^ | $/,""); print}')"
if [ "$(whichalt hwclock)" != 'hwclock not found' ]; then $(whichalt hwclock) -w; fi
if [ "$(whichalt timedatectl)" != 'timedatectl not found' ]; then $(whichalt timedatectl) set-local-rtc 0; fi
if [ "$(whichalt timedatectl)" != 'timedatectl not found' ]; then $(whichalt timedatectl); fi

echo "-----------------"
echo "Modernize sources"
echo "-----------------"
#if [ "$(whichalt apt)" != 'apt not found' ]; then $(whichalt apt) -y modernize-sources

echo "-----------------"
echo "Securing packages"
echo "-----------------"
if [ -d /etc/apt ]; then
#sed -i 's/http.kali.org/kali.download/g' /etc/apt/sources.list
if [ -f /etc/apt/sources.list ]; then echo "$(awk 'gsub(/http.kali.org/, "kali.download"); { print }' /etc/apt/sources.list)" > /etc/apt/sources.list; fi
find /etc/apt/sources.list* -type f -exec sed -i -e 's/http:/https:/g' {} 2>/dev/null \;
fi

echo "------------------"
echo "Getting keys"
echo "------------------"
#sudo apt update 2>&1 1>/dev/null | sed -ne 's/.*NO_PUBKEY //p' | while read key; do if ! [[ ${keys[*]} =~ "$key" ]]; then sudo gpg  --keyserver hkps://keyring.debian.org --recv-keys "$key"; keys+=("$key"); fi; done

echo "------------------"
echo "Cleaning repo"
echo "------------------"
if [ "$(whichalt apt)" != 'apt not found' ]; then $(whichalt apt) -y clean autoclean; fi

echo "---------------------------"
echo "Refreshing Packages from repo"
echo "---------------------------"
if [ -d /var/lib/apt ]; then rm -r /var/lib/apt/lists/*; fi
if [ "$(whichalt apt)" != 'apt not found' ]; then $(whichalt apt) -y update; fi

echo "-------------------"
echo "Fix Missing Depends"
echo "-------------------"
if [ "$(whichalt ps)" != 'ps not found' ]; then
aptpid=$(ps -ef | awk '!/print/' | awk '/apt -/ {print $2}')
if [ ! -z "$aptpid" ]; then printf "killing\n$aptpid"; apid=''; for apid in ${aptpid}; do kill -9 "${apid}"; done fi
fi
if [ "$(whichalt apt)" != 'apt not found' ]; then $(whichalt apt) -y --fix-broken install; fi

echo "------------------"
echo "Upgrading Packages"
echo "------------------"
#sudo apt -y full-upgrade
if [ "$(whichalt apt)" != 'apt not found' ]; then DEBIAN_FRONTEND=noninteractive apt -y -o Dpkg::Options::="--force-confnew" full-upgrade; fi

echo "---------------------"
echo "Removing Old Packages"
echo "---------------------"
if [ "$(whichalt apt)" != 'apt not found' ]; then $(whichalt apt) -y autoremove; fi

    if whichalt unattended-upgrades >/dev/null; then
    echo "unattended-upgrades exist"
    else
    #unattended-upgrades
    echo -n "Keep Packages Updated? (y/N) "
    read ans;

    case $ans in
        y|Y)
            apt -y install unattended-upgrades;;
        n|N)
            echo "No Update. Recommended to install upgrades";;
        *)
            echo "No update";;
    esac
    fi



fi #END ONLINE CHECK


#daily update
mkdir -p /etc/cron.daily
cfile=`realpath $0`
cat > /etc/cron.daily/update-system <<EOF
#!/bin/env bash
echo "Cleaning up logs"
$(whichalt journalctl) --vacuum-time=2d
echo "n" | sh $cfile
echo "Rebooting System on Daily Cron on update-system..."
sleep 5
echo b > /proc/sysrq-trigger
$(whichalt systemctl) reboot
$(whichalt reboot)
EOF
chmod 755 /etc/cron.daily/update-system

#weekly reboot
mkdir -p /etc/cron.weekly
cfile=`realpath $0`
cat > /etc/cron.weekly/update-system-reboot <<EOF
#!/bin/env bash
echo "n" | sh $cfile
#halt --reboot
echo b > /proc/sysrq-trigger
EOF
chmod 755 /etc/cron.weekly/update-system-reboot

#Startup and sync time
mkdir -p /etc/cron.startup
cfile=`realpath $0`
cat > /etc/cron.startup/update-time <<EOF
#!/bin/env bash
echo "Syncing Time"

curlbin=$(whichalt curl)
onlineserver=\$($curlbin -sI --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 https://1.1.1.1)
if [ -z "\$onlineserver" ]; then onlineserver=\$($curlbin -sI --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 https://9.9.9.9); fi
if [ -z "\$onlineserver" ]; then onlineserver=\$($curlbin -sI --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 https://[2606:4700:4700::1111]); fi
if [ -z "\$onlineserver" ]; then onlineserver=\$($curlbin -sI --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 https://[2620:fe::fe]); fi
if [ -z "\$onlineserver" ]; then onlineserver=\$($curlbin -sI --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 https://nist.time.gov); fi

if [ "$(whichalt timedatectl)" != 'timedatectl not found' ]; then $(whichalt timedatectl) set-ntp false; fi
#date -s "\$(curl --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 -sI 'https://time.cloudflare-dns.com/' 2>/dev/null | grep -i '^date:' | sed 's/^[Dd]ate: //g')"
#date -s "\$(curl --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print \$2}')"
if [ "$(whichalt date)" != 'date not found' ]; then $(whichalt date) -s "\$(echo "\$onlineserver" | $(whichalt awk) '/[dD]ate: / { \$1=""; print }' | $(whichalt awk) '{gsub(/^ | \$/,""); print}')"; fi

EOF
chmod 755 /etc/cron.startup/update-time

if [ ! -d /etc/cron.minutes ]; then mkdir -p /etc/cron.minutes; fi
cat > /etc/cron.minutes/dhclient <<EOF
#!/bin/env bash
debugecho="onerror"
if [ "\$debugecho" != "onerror" ]; then echo "DHCP Client Startup"; fi
lgint=\$(ip addr | awk -F: '/state/ && !/lo|docker0/ { gsub(/[ \t]/,"",\$2); print \$2 }')
while true
do
checkgtw=\$(ip -4 route | awk '/default/' && ip -6 route | awk '/default/')
if [ -z "\$checkgtw" ]; then
echo "/etc/cron.startup/dhclient - gtw not found getting ip dhcp"
$(whichalt dhclient) -1 2>/dev/null
sleep 60
else
if [ "\$debugecho" != "onerror" ]; then echo "gtw found"; fi
break
fi
done
EOF
chmod 755 /etc/cron.minutes/dhclient
if [ ! -d /etc/cron.startup ]; then mkdir -p /etc/cron.startup; fi
if [ ! -f /etc/cron.startup/dhclient ]; then cp /etc/cron.minutes/dhclient /etc/cron.startup/dhclient; fi

#[ -e /var/run/reboot-required ] && echo "******REBOOT NEEDED******"
if [ -e /var/run/reboot-required ]; then
 #reboot
 echo -n "Reboot Needed... Reboot? (Y/n) "
 read ans;
 case $ans in
    y|Y)
        echo "Rebooting..." & halt --reboot;;
    n|N)
        echo "******PLEASE REBOOT******";;
    *)
        echo "Rebooting..." & echo b > /proc/sysrq-trigger;;
 esac
 fi

echo "-----------------"
echo "Restarting services"
echo "-----------------"

##make all packages restart service
#if [ "$(whichalt debconf-set-selections)" != 'debconf-set-selections not found' ]; then echo '* libraries/restart-without-asking boolean true' | debconf-set-selections; fi

#if whichalt needrestart >/dev/null; then
##check for reboot
#sed -i 's/#$nrconf{restart} = \x27i\x27/$nrconf{restart} = \x27a\x27/g' /etc/needrestart/needrestart.conf
#DEBIAN_FRONTEND=noninteractive needrestart -r l
#else
#echo "installing package needrestart"
#if [ "$(whichalt apt)" != 'apt not found' ]; then $(whichalt apt) -y install needrestart; fi
#fi


echo "-----------------"
echo "End update script"
echo "-----------------"

sleep 5