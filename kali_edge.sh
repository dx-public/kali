#!/bin/bash

echo "Checking if Microsoft Edge is installed"
installed=`dpkg-query -l | grep 'microsoft-edge'`

if [ ! -z "$installed" ]
then
echo "Microsoft Edge is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        installed='';;
    u|U)
        echo "uninstalling"
        apt -y remove microsoft-edge*;;
    *)
        installed='n';;
esac

fi

if [ -z "$installed" ]; then

echo "Installing utils"
apt install apt-transport-https ca-certificates curl software-properties-common wget -y

echo "Downloading Microsoft Edge"
#sudo apt -y install microsoft-edge-stable
curl -s -L https://go.microsoft.com/fwlink?linkid=2149051 -o /tmp/microsoftedge.deb

echo "Installing Edge"
dpkg -i /tmp/microsoftedge.deb

echo "Getting Keys"
#curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft-edge.gpg
#install -o root -g root -m 644 microsoft-edge.gpg /etc/apt/trusted.gpg.d/
#curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /usr/share/keyrings/microsoft-edge-archive-keyring.gpg
wget -O - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /usr/share/keyrings/microsoft.gpg

echo "Removing old repos"
rm /etc/apt/sources.list.d/microsoft-edge*.list

echo "Add Repo"
#sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge.list'
sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft.gpg] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge.list'
chattr +i /etc/apt/sources.list.d/microsoft-edge.list
fi


echo "DONE going back to menu"
