#!/bin/bash
fdomainname=$(domainname -f)
fhostname=$(hostname -f)
echo ""
echo "Your full hostname is currently $(hostname -f)"
echo ""
echo -n "Enter New Full Hostname: "
read fhost

hname=`echo $fhost | cut -d'.' -f 1`
dname=`echo $fhost | cut -d'.' -f 2-`

if [ ! -z $fhost ]; then 
##cp -n /etc/hosts{,.old}
##idomainname=$(domainname -i)
#idomainname='127.0.1.1'
#newhn=$(cat /dev/urandom | tr -dc 'A-Z' | head -c8)
#mv /etc/hosts /etc/hosts.old
#echo "127.0.0.1  localhost" > /etc/hosts
#echo "$idomainname  $fhost   $hname" >> /etc/hosts
#echo "# The following lines are desirable for IPv6 capable hosts" >> /etc/hosts
#echo "::1     localhost ip6-localhost ip6-loopback" >> /etc/hosts
#echo "ff02::1 ip6-allnodes" >> /etc/hosts
#echo "ff02::2 ip6-allrouters" >> /etc/hosts

echo $hname > /etc/hostname
echo $dname > /etc/domainname
echo "${hname}.${dname}" > /etc/fqdn
hostnamectl set-hostname $hname
domainname -b $dname
#sysctl kernel.hostname=${hname}.${dname}
systemctl restart systemd-hostnamed
service networking force-reload
echo "Your new hostname is set to: $(hostname) and domain is $(domainname)"
echo "Full FQDN Domain: $(domainname -f)"
echo "Full FQDN Host:   $(hostname -f)"
hostnamectl
echo "You might need to logoff for settings to take effect (if they dont match)"
fi 

echo "DONE going back to menu"
