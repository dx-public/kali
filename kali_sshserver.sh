#!/bin/bash

whichalt(){
file=$1
found=0
#pathdir="$PATH"
#if [ -z "$pathdir" ]; then pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"; fi
pathdir="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/etc/$1/$1"
for p in $(echo "$pathdir" | awk '{gsub(/:/,"\n");print}')
 do
  if [ -e "${p}/${file}" ]; then found=1; echo "${p}/${file}"; return 0; fi
 done
if [ "$found" = 0 ]; then echo "$file not found"; return 1; fi
}

dohparse(){
domain=$1
type=$2
if [ -z "$type" ]; then type='A'; fi
curl="$(whichalt curl)"
data=""
data=$($curl "https://8.8.8.8/resolve?type=$type&name=$domain" 2>/dev/null)
if [ -z "$data" ]; then data=$($curl "https://[2001:4860:4860::8888]/resolve?type=$type&name=$domain" 2>/dev/null); fi
parse=$(echo $data | awk 'gsub(/,/,"\n") { print }' | awk '/data/ { print }' | awk 'gsub(/data|}|]|"/,"") { print $1 }' | awk '/[[:digit:]]/ {print}' | awk '{printf "%s\n",$0} END {print ""}' | awk 'gsub(/^:/,""); { print }' | awk 'gsub(/,$/,""); { print }' | awk '!seen[$0]++')
if [ "$type" = 'A' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}' | awk '!seen[$0]++' | awk '/./'; fi
if [ "$type" = 'AAAA' ]; then echo $parse | awk '{printf "%s ", $0}' | awk 'gsub(/ /," \n") { print }' | awk '/(([0-9a-fA-F]{0,4}:){1,7}[0-9a-fA-F]{0,4})/{print}' | awk '!/[g-zG-Z]/{print}' | awk '!seen[$0]++' | awk '/./'; fi
if [ "$type" = 'TXT' ]; then echo "$parse"; fi
#if [ ! -z "$parse" ]; then echo $parse; fi
}

check_installed(){
pkg=$1
if [ -z "$pkg" ]; then echo "please pass pkg"; exit; fi
checkpkg="$(whichalt $pkg)"

#if whichalt $pkg >/dev/null; then
if [ "$checkpkg" = "$pkg not found" ]; then
 if [ ! -z $2 ]; then pkg=$2; fi
 echo "installing $pkg"
 apt -y install $pkg
else
 echo "$pkg exists"
fi

if [ -z "$checkpkg" ]; then echo "please install $pkg manually"; exit; fi

}


echo "Checking if SSH is installed"
#installed=`dpkg-query -l | grep 'openssh-server'`
installed=$(check_installed sshd openssh-server)

#if [ ! -z "$installed" ]; then
#echo "OpenSSH is aleady installed"
#echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
#read ans;

#case $ans in
#    r|R)
#        installed='';;
#    u|U)
#        echo "disabling"
#        sudo systemctl disable ssh;;
#    *)
#        installed='n';;
#esac

#fi

#if [ -z "$installed" ]; then

#echo "Installing SSH Server"
#sudo apt -y install openssh-server
#sudo apt-mark auto openssh-server

#echo "Removing Previous SSH Defaults"
#sudo update-rc.d -f ssh remove

#echo "Updating SSH Defaults"
#sudo update-rc.d -f ssh defaults

echo "Updating Keys"
if [ ! -d /etc/ssh/insecure_original_default_keys ]; then
 mkdir -p /etc/ssh/insecure_original_default_keys
 mv /etc/ssh/ssh_host_* /etc/ssh/insecure_original_default_keys/
fi
if [ -d /etc/ssh/insecure_original_default_keys ]; then
 rm -rf /etc/ssh/ssh_host_*
fi

#dpkg-reconfigure openssh-server
#Using default values to regenerate ssh keys
ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key
ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key
ssh-keygen -q -N "" -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
ssh-keygen -q -N "" -t ed25519 -f /etc/ssh/ssh_host_ed25519_key
#Uncomment for more secure ssh
#ssh-keygen -q -N "" -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key


echo "change line PermitRootLogin without-password if applicable (yes)"

echo "Securing SSH"
echo "$(awk ' { gsub(/#MaxAuthTries 6/, "MaxAuthTries 1"); print }' /etc/ssh/sshd_config)" > /etc/ssh/sshd_config
echo "$(awk ' { gsub(/#MaxSessions 10/, "MaxSessions 1"); print }' /etc/ssh/sshd_config)" > /etc/ssh/sshd_config

echo "Restarting SSH"
#sudo service ssh restart
#sudo update-rc.d -f ssh enable 2 3 4 5
systemctl enable --now sshd 2>/dev/null
systemctl restart sshd 2>/dev/null
systemctl enable --now ssh 2>/dev/null
systemctl restart ssh 2>/dev/null
systemctl enable --now openssh-server 2>/dev/null
systemctl restart openssh-server 2>/dev/null

echo "Feel Free to change the MOTD (/etc/motd) if you want and then reboot ssh server"
#fi

#echo "Checking if kdig is installed"
#installed=`dpkg-query -l | grep 'knot-dnsutils'`
#if [ -z "$installed" ]; then apt -y install knot-dnsutils; fi

echo "Updating SSH Public key"
#ssh-keygen -t rsa -f dnskey -b 4096; cat dnskey.pub
#pubkey=`kdig @9.9.9.9 +tls +short TXT key-ssh-rsa.$(hostname -f | cut -d'.' -f2-) | sed -e 's/" "//g' | tr -d '\n' | sed '$s/$/\n/' | tr -d '"'`
pubkey=$(dohparse key-ssh-rsa.$(hostname -d) TXT)

echo "checking key"
asum=`echo $pubkey | awk '{ print $1,$2 }' | sha256sum | awk '{ print $1 }'`
psum=`echo $pubkey | awk '{ print $3 }' | cut -d'-' -f1`
if [ "$psum"=="$asum" ]; then 
mkdir -p ~/.ssh/
echo "$pubkey" > ~/.ssh/authorized_keys
fi


#echo "Checking if Fail2Ban is installed"
#installed=`dpkg-query -l | grep 'fail2ban'`

#if [ -z "$installed" ]; then
echo "(Re)Installing Fail2ban..."
 apt -y install fail2ban
#fi
rm -rf /etc/fail2ban/jail.d/default*
cat /etc/fail2ban/jail.d/ssh.conf<<'EOF'
# Block attempts to brute force SSH logins
[sshd]
enabled = true
port = ssh
filter = sshd
logpath = /var/log/auth.log
maxretry = 1
bantime = 86100
ignoreip = 127.0.0.1 ::1
EOF
echo "starting fail2ban service"
sudo systemctl enable --now fail2ban.service
sudo systemctl restart fail2ban.service
sleep 2
sudo fail2ban-client status sshd
echo "to unban: sudo fail2ban-client set sshd unbanip 127.0.0.1"

echo "DONE going back to menu"
