#!/bin/bash

echo "Checking if Docker is installed"
installed=`dpkg-query -l | grep 'docker.io'`

if [ ! -z "$installed" ]; then
 echo "Docker is aleady installed"
 echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
 read ans;

 case $ans in
    r|R)
        installed='';;
    u|U)
        echo "uninstalling"
        apt -y remove docker.io*;;
    *)
        installed='n';;
 esac
fi

if [ -z "$installed" ]; then
 echo "Installing Docker"
 sudo apt -y install docker.io
fi

if [ ! -d /etc/docker ]; then
 mkdir -p /etc/docker
fi

echo "creating update script"
cat > /etc/docker/update-docker.sh <<'EOF'
#!/usr/bin/env bash
set -e

# Function to preserve and update container
preserve_and_update_container() {
    local container=$1
    local image=$(docker inspect --format '{{.Config.Image}}' "$container")

    # Pull the latest image version
    docker pull $image

    # Compare image IDs to determine if an update is needed
    local latest_image_id=$(docker inspect --format '{{.Id}}' $image)
    local container_image_id=$(docker inspect --format '{{.Image}}' "$container")

    if [[ "$latest_image_id" != "$container_image_id" ]]; then
        echo "Updating $container..."

        # Capture current configurations
        local env_vars=$(docker inspect $container --format '{{range .Config.Env}}{{println .}}{{end}}')
        local volumes=$(docker inspect $container --format '{{range .Mounts}}{{println .Source ":" .Destination}}{{end}}')
        local network=$(docker network ls --filter id=$(docker inspect $container --format '{{.HostConfig.NetworkMode}}') --format '{{.Name}}')

        # Remove the outdated container
        docker rm -f $container

        # Recreate the container with the same configurations
        docker run -d --name $container $(echo "$env_vars" | xargs -I {} echo --env '{}') $(echo "$volumes" | xargs -I {} echo -v '{}') --network="$network" $image
        echo "$container updated successfully."
    else
        echo "$container is already up to date."
    fi
}

# Iterate over all running containers
for container in $(docker ps --format "{{.Names}}"); do
    preserve_and_update_container $container
done

echo "Container update check complete while preserving existing container configurations."
EOF

chmod +x /etc/docker/update-docker.sh

if [ ! -f /etc/docker/daemon.json ]; then
cat <<EOF > /etc/docker/daemon.json
{
  "live-restore": true,
  "storage-driver": "overlay2",
  "log-opts": {
    "max-size": "10m"
  }
}
EOF
fi

if [ ! -f /etc/docker-compose/docker-compose ]; then
mkdir -p /etc/docker-compose
mkdir -p /usr/local/lib/docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/latest/download/docker-compose-linux-x86_64 -o /etc/docker-compose/docker-compose
chmod +x /etc/docker-compose/docker-compose
ln -s /etc/docker-compose/docker-compose /usr/local/lib/docker/cli-plugins/docker-compose
fi

echo "DONE going back to menu"
