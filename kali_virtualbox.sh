#!/bin/bash

echo "Import Key"
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- \
  | gpg --dearmor \
  | sudo tee /usr/share/keyrings/virtualbox-archive-keyring.gpg

echo "Adding it to sources.list"
  echo "deb [arch=amd64 signed-by=/usr/share/keyrings/virtualbox-archive-keyring.gpg] https://download.virtualbox.org/virtualbox/debian buster contrib" \
  | sudo tee /etc/apt/sources.list.d/virtualbox.list

echo "refresh update list"
sudo apt update

echo "keep kernel modules up-to-date"
sudo apt install -y dkms

echo "Installing virtualbox"
sudo apt install -y virtualbox virtualbox-ext-pack

echo "fixing permissions"
userq=`cat /etc/passwd | grep '/home/' | grep -v 'nologin' | tail -1 | cut -d':' -f1`
usermod -aG vboxusers $userq

echo "create autorun"
mkdir -p /etc/cron.startup
touch /etc/cron.startup/vbox-autostartup
echo '#!/bin/sh' > /etc/cron.startup/vbox-autostartup
echo 'echo "starting vbox.."' >> /etc/cron.startup/vbox-autostartup
echo 'systemctl start virtualbox' >> /etc/cron.startup/vbox-autostartup
echo 'sleep 10' >> /etc/cron.startup/vbox-autostartup
echo 'DISPLAY=:0' >> /etc/cron.startup/vbox-autostartup
echo "sudo -H -u $userq /usr/bin/vboxmanage startvm 'VMNAME' --type headless" >> /etc/cron.startup/vbox-autostartup
echo "su -l $userq --command 'VBoxHeadless --startvm VMNAME'" >> /etc/cron.startup/vbox-autostartup
echo "su -l $userq --command 'VBoxManage startvm VMNAME --type headless'" >> /etc/cron.startup/vbox-autostartup
echo 'echo "DONE"' >> /etc/cron.startup/vbox-autostartup 

chmod +x /etc/cron.startup/vbox-autostartup

echo "DONE going back to menu"
